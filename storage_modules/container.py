from itertools import tee
import uuid



def delete_container(blob_service_client, container_name):
    try:
        print("- Deleting container ID ", container_name)
        container_client = blob_service_client.get_container_client(container = container_name)
        container_client.delete_container()
    except Exception as e:
        print("! Unable to delete container: {}".format(e))
    else:
        print("! Deleted container {} successfully!".format(container_name))



# Print out all the containers and return a list of container names
def list_containers(blob_service_client, silent = False):
    # Check if any containers are already present
    # list_containers() returns a custom iterator called ItemPaged.
    all_containers = blob_service_client.list_containers(include_metadata=True)

    # Iterators can only be used once. So if we want to acquire the count of
    # containers, we need to convert it into a list. This breaks the iterator.
    # To prevent this, just use tee() to make a copy of the iterator.
    all_containers, iter_copy2 = tee(all_containers)

    # Create a list of container names which can be used later
    container_list = []
    container_count = len(list(iter_copy2))

    if container_count <= 0:
        if not silent: print("! No containers found.")
        return []
    else:
        if not silent: print("! {} container(s) found".format(container_count))
        for container in all_containers:
            container_list.append(str(container['name']))
            if not silent: print(">", container['name'], ' | ' ,container['last_modified'])

    return container_list



def create_container(blob_service_client, container_list = []):

        # Generate a unique ID for a container name
        new_container_name = str(uuid.uuid4())
        print("- Creating container ID {}".format(new_container_name))

        try:
            # TODO: There seems to be no use of container_client?
            container_client = blob_service_client.create_container(new_container_name)
        except Exception as err:
            print("Container creation failed: {}".format(err))
        else:
            print("! Container {} created successfully".format(new_container_name))



def container_select(container_list):
    ind = 1
    for container in container_list:
        print("{}. {}".format(ind, container))
        ind += 1
    container_index = int(input(">> "))
    try:
        return container_list[container_index - 1]
    except:
        return None