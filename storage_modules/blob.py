import uuid
import os.path, os

def create_blob(blob_service_client, container_name, file_path):
    if os.path.isfile(file_path):
        file_extension = file_path.split(".")[-1]
        blob_name = str(uuid.uuid4()) + "." + file_extension
        blob_client = blob_service_client.get_blob_client(container = container_name, blob = blob_name)

        print("- Uploading file {} to container {}".format(blob_name, container_name))
        
        try:
            with open(file_path, 'rb') as data:
                blob_client.upload_blob(data)
        except Exception as err:
            print("! Blob creation failed: {}".format(err))
        else:
            print("! Blob {} created successfully".format(blob_name))
    else:
        print("! File path is invalid / File does not exist.")



def list_blobs(blob_service_client, container_name, silent = False):
    container_client = blob_service_client.get_container_client(container = container_name) 
    if not silent: print("\n! Container {} contains: ".format(container_name))
    blob_list = container_client.list_blobs()

    return_list = []
    for blob in blob_list:
        return_list.append(blob.name)
        if not silent: print("# {}".format(blob.name))

    return return_list
    

def download_blob(blob_service_client, container_name, blob_name):
    filepath = str(input(">> Save folder: ")).lower()

    if not os.path.exists(filepath):
        print("- Creating folder path /", filepath)
        os.makedirs(filepath)

    # initialize blob client
    print("- Downloading file ", blob_name)
    blob_client = blob_service_client.get_blob_client(container = container_name, blob = blob_name)
    with open(filepath + "\\" + blob_name, mode = "wb") as sample_blob:
        download_stream = blob_client.download_blob()
        sample_blob.write(download_stream.readall())
    print("- File {} downloaded successfully\n".format(blob_name))

def blob_select(blob_list):
    ind = 1
    for container in blob_list:
        print("{}. {}".format(ind, container))
        ind += 1
    container_index = int(input(">> "))
    try:
        return blob_list[container_index - 1]
    except:
        return None

def delete_blob(blob_service_client, container_name, blob_name):
    blob_client = blob_service_client.get_blob_client(container = container_name, blob = blob_name)
    try:
        print("- Deleting blob {}".format(blob_name))
        blob_client.delete_blob()
    except Exception as err:
        print("! Unable to delete blob {}".format(err))
    else:
        print("! Blob {} deleted successfully".format(blob_name))