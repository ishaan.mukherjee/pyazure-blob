

from storage_modules.container import list_containers, container_select, create_container, delete_container
from storage_modules.blob import create_blob, list_blobs, blob_select, download_blob, delete_blob

import pathlib



# global variables
container_list  = []



def help_gui():
    print("Containers: `container ls`, `container show`, `container create`, `container rm")
    print("Blobs: `blob create <file_path>`, `blob fetch`, `blob rm`\n")


def update_container_list(client):
    global container_list
    container_list = list_containers(client, silent = True)


def cli_gui(client):
    # This is going to be ugly

    # Initially populate the container list
    update_container_list(client)
    exit_loop = False

    while (exit_loop == False):

        inp = str(input("$ ")).lower().split()

        if inp[0] == 'help':
            help_gui()
        elif inp[0] == 'exit':
            exit_loop = True
        
        elif inp[0] == 'container':
            if inp[1] == 'ls':
                # list containers
                global container_list
                container_list = list_containers(client)
                
            elif inp[1] == 'show':
                if len(container_list) > 0:
                    container = container_select(container_list)
                    blob_list = list_blobs(client, container)
                else:
                    print("No containers found.\n")

            elif inp[1] == 'create':
                create_container(client, container_list)
                update_container_list(client)
                
            elif inp[1] == 'rm':
                if len(container_list) > 0:
                    container = container_select(container_list)
                    delete_container(client, container)
                    update_container_list(client)
                else:
                    print("No containers found.\n") 
            
            else:
                print("! Unknown command. Please try `help` for a list of commands.")
                


        elif inp[0] == 'blob':
            if inp[1] == 'create':
                print("CWD: {}".format(pathlib.Path().resolve()))
                try:
                    container = container_select(container_list)
                    create_blob(client, container, inp[2])
                except IndexError:
                    print("! File path not provided.")
                
            elif inp[1] == 'fetch':
                if len(container_list) > 0:
                    # download a blob from container
                    print("CWD: {}".format(pathlib.Path().resolve()))

                    container = container_select(container_list)
                    blob_list = list_blobs(client, container, silent = True)
                    blob = blob_select(blob_list)
                    download_blob(client, container, blob)
                else:
                    print("No containers found.\n") 
                       
            elif inp[1] == 'rm':
                if len(container_list) > 0:
                    container = container_select(container_list)
                    blob_list = list_blobs(client, container, silent = True)
                    blob = blob_select(blob_list)
                    delete_blob(client, container, blob)
                else:
                    print("No containers found.\n") 
     
                
            else:
                print("! Unknown command. Please try `help` for a list of commands.")
                
        else:
            print("! Unknown command. Please try `help` for a list of commands.")



