from dotenv import load_dotenv, find_dotenv
import os
# import blob storage methods
from storage_modules.container import list_containers, create_container
from storage_modules.blob import list_blobs, create_blob

# import CLI methods
from gui.gui import cli_gui


# azure credentials & blob storage methods
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient


if __name__ == "__main__":


    # load the environment variables
    load_dotenv(find_dotenv())
    CONN_STRING = os.getenv("CONN_STRING")

    # uuid() keeps throwing a user warning if you use python 32 bit.
    # It's really annoying. So, just clear the screen so this program's output
    # is more readable.
    print("\n" * 10)
    try:
        
        # Connect to the Azure Blob Service Client using a connection string
        # NOTE: Passwordless approach just didn't seem to work after nearly 10 attempts. Troubleshooting didn't help either.
        print("> Creating Blob Service Client...\n")

        blob_service_client = BlobServiceClient.from_connection_string(CONN_STRING)

        # interface        
        print("!! Type 'help' for the list of commands")
        cli_gui(blob_service_client)
        

    except Exception as e:
        print("Something went wrong!: {}".format(e))